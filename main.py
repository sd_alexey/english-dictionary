import json
from difflib import get_close_matches

from kivy.app import App
from kivy.uix.textinput import TextInput
from kivy.uix.label import Label
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button


class MysApp(App):
    data = json.load(open("data.json"))

    def build(self):

        bl = BoxLayout(orientation='vertical', padding=10)
        gl = GridLayout(cols=4, spacing=10, size_hint=(1, 0.1))
        gl.add_widget(Label(text='Enter a word:'))
        gl.add_widget(TextInput(multiline=False, on_text_validate=self.retrive_definition))
        gl.add_widget(Button(text='Clear', on_press=self.clear))

        self.serch = BoxLayout(orientation='vertical')
        self.serch.clear_widgets()
        self.lbl = Label(font_size=16,
                         text='',
                         halign='left',
                         valign='top',
                         text_size=(500, 500))
        self.serch.add_widget(self.lbl)
        bl.add_widget(gl)
        bl.add_widget(self.serch)

        return bl

    def retrive_definition(self, instance):
        word = instance.text.lower()

        if word in self.data:
            self.form_word(self.data[word])
        elif word.title() in self.data:
            self.form_word(self.data[word.title()])
        elif word.upper() in self.data:
            self.form_word(self.data[word.upper()])
        elif len(get_close_matches(word, self.data.keys())) > 0:
            self.do_list(get_close_matches(word, self.data.keys(), 5, 0.4))

    def form_word(self, word):
        if type(word) == list:
            output = ''
            n = 0
            for item in word:
                n = n+1
                output = output + '\n' + str(n) + ') ' + item
            self.lbl.text = output
        else:
            self.lbl.text = str(word)

    def do_list(self, words):
        self.serch.clear_widgets()
        self.serch.add_widget(Label(text='Did you mean instead?'))
        for word in words:
            self.serch.add_widget(Button(text=word, on_press=self.one_press_button, size_hint=(0.3, 1)))

    def one_press_button(self, instance):
        self.serch.clear_widgets()
        self.lbl = Label(font_size=16,
                         text='',
                         halign='left',
                         valign='top',
                         text_size=(500, 500))
        self.serch.add_widget(self.lbl)
        self.form_word(self.data[instance.text])

    def clear(self, instance):
        self.serch.clear_widgets()

if __name__ == '__main__':
    MysApp().run()
