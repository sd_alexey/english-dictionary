import json
import difflib

from difflib import get_close_matches

data = json.load(open("data.json"))

def retrive_definition(word):
	word = word.lower()
	
	if word in data:
		return data[word]
	elif word.title() in data:
		return data[word.title()]
	elif word.upper() in data:
		return data[word.upper()]
	elif len(get_close_matches(word, data.keys())) > 0:
		action = input('Did you mean %s instead?' % get_close_matches(word, data.keys()))
		if (int(action)):
			return data[get_close_matches(word, data.keys())[int(action)-1]]
		elif (action == 'n'):
			return ('The word doesnt exist, yet.')
		else:
			return ('We dont understand your entry. Apologies')

word_user = input('Enter a word: ')

output = retrive_definition(word_user)
if type(output) == list:
	for item in output:
		print('-', item)
else:
	print(output)